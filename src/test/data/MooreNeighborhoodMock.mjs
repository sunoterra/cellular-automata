const Limits = [2, 2]

const EmptyMock = {
  top: {
    left: {
      coordinates: [0, 0],
      e: [0, 1],
      n: [2, 0],
      ne: [2, 1],
      nw: [2, 2],
      s: [1, 0],
      se: [1, 1],
      sw: [1, 2],
      w: [0, 2]
    },
    middle: {
      coordinates: [0, 1],
      e: [0, 2],
      n: [2, 1],
      ne: [2, 2],
      nw: [2, 0],
      s: [1, 1],
      se: [1, 2],
      sw: [1, 0],
      w: [0, 0]
    },
    right: {
      coordinates: [0, 2],
      e: [0, 0],
      n: [2, 2],
      ne: [2, 0],
      nw: [2, 1],
      s: [1, 2],
      se: [1, 0],
      sw: [1, 1],
      w: [0, 1]
    }
  },
  center: {
    left: {
      coordinates: [1, 0],
      e: [1, 1],
      n: [0, 0],
      ne: [0, 1],
      nw: [0, 2],
      s: [2, 0],
      se: [2, 1],
      sw: [2, 2],
      w: [1, 2]
    },
    middle: {
      coordinates: [1, 1],
      e: [1, 2],
      n: [0, 1],
      ne: [0, 2],
      nw: [0, 0],
      s: [2, 1],
      se: [2, 2],
      sw: [2, 0],
      w: [1, 0]
    },
    right: {
      coordinates: [1, 2],
      e: [1, 0],
      n: [0, 2],
      ne: [0, 0],
      nw: [0, 1],
      s: [2, 2],
      se: [2, 0],
      sw: [2, 1],
      w: [1, 1]
    }
  },
  bottom: {
    left: {
      coordinates: [2, 0],
      e: [2, 1],
      n: [1, 0],
      ne: [1, 1],
      nw: [1, 2],
      s: [0, 0],
      se: [0, 1],
      sw: [0, 2],
      w: [2, 2]
    },
    middle: {
      coordinates: [2, 1],
      e: [2, 2],
      n: [1, 1],
      ne: [1, 2],
      nw: [1, 0],
      s: [0, 1],
      se: [0, 2],
      sw: [0, 0],
      w: [2, 0]
    },
    right: {
      coordinates: [2, 2],
      e: [2, 0],
      n: [1, 2],
      ne: [1, 0],
      nw: [1, 1],
      s: [0, 2],
      se: [0, 0],
      sw: [0, 1],
      w: [2, 1]
    }
  }
}

export { Limits, EmptyMock }
