'use strict'
const { Runner } = require('mocha')

const COLORS = {
  bg: {
    blue: '\x1b[44m',
    cyan: '\x1b[46m',
    green: '\x1b[42m',
    red: '\x1b[41m',
    yellow: '\x1b[43m'
  },
  fg: {
    blue: '\x1b[34m',
    cyan: '\x1b[36m',
    green: '\x1b[32m',
    red: '\x1b[31m',
    yellow: '\x1b[33m'
  },
  reset: '\x1b[0m'
}

const {
  constants: {
    EVENT_RUN_BEGIN,
    EVENT_RUN_END,
    EVENT_TEST_FAIL,
    EVENT_TEST_PASS,
    EVENT_SUITE_BEGIN,
    EVENT_SUITE_END
  }
} = Runner

class ColorCorrected {
  #indents = 0
  constructor(runner) {
    const { stats } = runner

    runner
      .once(EVENT_RUN_BEGIN, () => {
        // console.log('Cellular Automaton')
      })
      .on(EVENT_SUITE_BEGIN, (suite) => {
        const {
          fg: { blue },
          reset
        } = COLORS
        console.group(`${blue}${suite.title}${reset}`)
      })
      .on(EVENT_SUITE_END, () => {
        console.groupEnd()
      })
      .on(EVENT_TEST_PASS, (test) => {
        const {
          fg: { cyan, green },
          reset
        } = COLORS
        console.log(`${green}${reset} ${cyan}${test.title}${reset}`)
      })
      .on(EVENT_TEST_FAIL, (test, thrown) => {
        const {
          fg: { red, yellow },
          reset
        } = COLORS
        console.log(`${red}${reset} ${test.title}`)
        console.log(`${yellow}- ${thrown.message}${reset}`)
      })
      .once(EVENT_RUN_END, () => {
        const {
          fg: { blue, green, red },
          reset
        } = COLORS
        console.group()
        console.log(
          `\n${blue}Results${reset} ${green}${reset} ${stats.passes} ${red}${reset} ${stats.failures}\n`
        )
        console.groupEnd()
      })
  }
}

module.exports = ColorCorrected
