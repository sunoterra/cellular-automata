import World from './World.mjs'

export default class Origin extends World {
  // origin x/y coordinates
  static x = -1
  static y = -1

  static set([y, x]) {
    if (y < this.i) {
      throw new Error('invalid origin y-coordinate')
    }
    if (x < this.j) {
      throw new Error('invalid origin x-coordinate')
    }
    this.y = y
    this.x = x
  }
}
