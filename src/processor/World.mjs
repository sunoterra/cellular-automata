export default class World {
  // lower-limits
  static i = 0
  static j = 0
  // upper-limits
  static m = -1
  static n = -1

  static boundaries([m, n]) {
    if (isNaN(m) || m < this.i) {
      throw new Error('invalid y-coordinate upper-limit')
    }
    if (isNaN(n) || n < this.j) {
      throw new Error('invalid x-coordinate upper-limit')
    }
    this.m = m
    this.n = n
  }
}
