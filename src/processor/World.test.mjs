import { Limits } from '../test/data/MooreNeighborhoodMock.mjs'
import World from './World.mjs'
import { assert } from 'chai'
import mocha from 'mocha'

const { before, describe, it } = mocha

describe('processor/World', () => {
  describe('static lower-limits [i, j]', () => {
    it('should have World.i = 0 and World.j = 0', () => {
      assert.equal(World.i, 0, 'a of i')
      assert.equal(World.j, 0, 'a of j')
    })
  })
  describe('static upper-limits [m, n]', () => {
    before(() => {
      World.boundaries(Limits)
    })
    it(`should return set boundaries e.g. ${JSON.stringify(Limits)}`, () => {
      assert.equal(World.m, 2, 'a of m')
      assert.equal(World.n, 2, 'a of n')
    })
  })
})
