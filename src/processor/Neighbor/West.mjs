import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class West extends Origin {
  constructor() {
    super()
    // [y = [i..m], j]
    if (Origin.x === World.j) {
      // [y = [i..m], n]
      return [Origin.y, World.n]
    }
    // [y = [i..m], x = [j + 1..n] - 1]
    return [Origin.y, Origin.x - 1]
  }
}
