import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class East extends Origin {
  constructor() {
    super()
    // [y = [i..m], n]
    if (Origin.x === World.n) {
      // [y = [i..m], j]
      return [Origin.y, World.j]
    }
    // [y = [i..m], x = [j..n - 1] + 1]
    return [Origin.y, Origin.x + 1]
  }
}
