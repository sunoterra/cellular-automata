import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class NorthWest extends Origin {
  constructor() {
    super()
    // [i, x = [j..n]]
    if (Origin.y === World.i) {
      // [i, j]
      if (Origin.x === World.j) {
        // [m, n]
        return [World.m, World.n]
      }
      // [i, x = [j + 1..n]]
      if (Origin.x !== World.j) {
        // [m, x = [j + 1..n] - 1]
        return [World.m, Origin.x - 1]
      }
    }
    // [y = [i + 1..m], j]
    if (Origin.y !== World.i && Origin.x === World.j) {
      // [y = [i + 1..m] - 1, n]
      return [Origin.y - 1, World.n]
    }
    // [y = [i + 1..m] - 1, x = [j + 1..n] - 1]
    return [Origin.y - 1, Origin.x - 1]
  }
}
