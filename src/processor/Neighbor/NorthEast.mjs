import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class NorthEast extends Origin {
  constructor() {
    super()
    // [i, x = [j..n]]
    if (Origin.y === World.i) {
      // [i, n]
      if (Origin.x === World.n) {
        // [m, j]
        return [World.m, World.j]
      }
      // [i, x = [j..n - 1]]
      if (Origin.x !== World.n) {
        // [m, x = [j..n - 1] + 1]
        return [World.m, Origin.x + 1]
      }
    }
    // [y = [i + 1..m], n]
    if (Origin.y !== World.i && Origin.x === World.n) {
      // [y = [i + 1..m] - 1, j]
      return [Origin.y - 1, World.j]
    }
    // [y = [i + 1..m] - 1, x = [j..n - 1] + 1]
    return [Origin.y - 1, Origin.x + 1]
  }
}
