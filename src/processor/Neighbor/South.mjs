import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class South extends Origin {
  constructor() {
    super()
    // [m, x = [j..n]]
    if (Origin.y === World.m) {
      // top edge [i, x = [j..n]]
      return [World.i, Origin.x]
    }
    // otherwise [y = [i..m - 1] + 1, x = [j..n]]
    return [Origin.y + 1, Origin.x]
  }
}
