import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class North extends Origin {
  constructor() {
    super()
    // [i, x = [j..n]]
    if (Origin.y === World.i) {
      // [m, x = [j..n]]
      return [World.m, Origin.x]
    }
    // [y = [i + 1..m] - 1, x = [j..n]]
    return [Origin.y - 1, Origin.x]
  }
}
