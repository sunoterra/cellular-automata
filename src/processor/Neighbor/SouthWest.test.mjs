import { EmptyMock, Limits } from '../../test/data/MooreNeighborhoodMock.mjs'
import Origin from '../Origin.mjs'
import SouthWest from './SouthWest.mjs'
import World from '../World.mjs'
import { assert } from 'chai'
import mocha from 'mocha'

const { beforeEach, describe, it } = mocha
const { bottom, center, top } = EmptyMock

describe('processor/Neighbor/SouthWest', () => {
  beforeEach(() => {
    World.boundaries(Limits)
  })

  describe(`row y = 0`, () => {
    const { left, middle, right } = top
    it(`should return center-row right [1, 2], for column x = 0`, () => {
      Origin.set(left.coordinates)
      assert.deepEqual(new SouthWest(), left.sw, 'center-row right')
    })
    it(`should return center-row left [1, 0], for column x = 1`, () => {
      Origin.set(middle.coordinates)
      assert.deepEqual(new SouthWest(), middle.sw, 'center-row left')
    })
    it(`should return center-row middle [1, 1], for column x = 2`, () => {
      Origin.set(right.coordinates)
      assert.deepEqual(new SouthWest(), right.sw, 'center-row middle')
    })
  })

  describe(`row y = 1`, () => {
    const { left, middle, right } = center
    it(`should return bottom-row right [2, 2], for column x = 0`, () => {
      Origin.set(left.coordinates)
      assert.deepEqual(new SouthWest(), left.sw, 'bottom-row right')
    })
    it(`should return bottom-row left [2, 0], for column x = 1`, () => {
      Origin.set(middle.coordinates)
      assert.deepEqual(new SouthWest(), middle.sw, 'bottom-row left')
    })
    it(`should return bottom-row middle [2, 1], for column x = 2`, () => {
      Origin.set(right.coordinates)
      assert.deepEqual(new SouthWest(), right.sw, 'bottom-row middle')
    })
  })

  describe(`row y = 2`, () => {
    const { left, middle, right } = bottom
    it(`should return top-row right [0, 2], for column x = 0`, () => {
      Origin.set(left.coordinates)
      assert.deepEqual(new SouthWest(), left.sw, 'top-row right')
    })
    it(`should return top-row left [0, 0], for column x = 1`, () => {
      Origin.set(middle.coordinates)
      assert.deepEqual(new SouthWest(), middle.sw, 'top-row left')
    })
    it(`should return top-row middle [0, 1], for column x = 2`, () => {
      Origin.set(right.coordinates)
      assert.deepEqual(new SouthWest(), right.sw, 'top-row middle')
    })
  })
})
