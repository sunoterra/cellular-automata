import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class SouthEast extends Origin {
  constructor() {
    super()
    // [m, x = [j..n]]
    if (Origin.y === World.m) {
      // [m, n]
      if (Origin.x === World.n) {
        // [i, j]
        return [World.i, World.j]
      }
      // [m, x = [j..n - 1]]
      if (Origin.x !== World.n) {
        // [i, x = [j..n - 1] + 1]
        return [World.i, Origin.x + 1]
      }
    }
    // [y = [i..m - 1], n]
    if (Origin.y !== World.m && Origin.x === World.n) {
      // [y = [i..m - 1] + 1, j]
      return [Origin.y + 1, World.j]
    }
    // [y = [i..m - 1] + 1, x = [j..n - 1] + 1]
    return [Origin.y + 1, Origin.x + 1]
  }
}
