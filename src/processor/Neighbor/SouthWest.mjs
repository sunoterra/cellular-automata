import Origin from '../Origin.mjs'
import World from '../World.mjs'

export default class SouthWest extends Origin {
  constructor() {
    // [m, x = [j..n]]
    if (Origin.y === World.m) {
      // [m, j]
      if (Origin.x === World.j) {
        // [i, n]
        return [World.i, World.n]
      }
      // [m, x = [j + 1..n]]
      if (Origin.x !== World.j) {
        // [i, x = [j + 1..n] - 1]
        return [World.i, Origin.x - 1]
      }
    }
    // [y = [i..m - 1], j]
    if (Origin.y !== World.m && Origin.x === World.j) {
      // [y = [i..m - 1] + 1, n]
      return [Origin.y + 1, World.n]
    }
    // [y = [i..m - 1] + 1, x = [j + 1..n] - 1]
    return [Origin.y + 1, Origin.x - 1]
  }
}
