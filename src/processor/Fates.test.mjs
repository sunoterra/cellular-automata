import Fates from './Fates.mjs'
import { assert } from 'chai'
import mocha from 'mocha'

const { describe, it } = mocha

const ALIVE = true
const CUT = false
const DEAD = false
const DRAWOUT = true

describe('processor/Fates', () => {
  const fates = new Fates()
  describe('when the loom method is envoked with no arguments, e.g. Fates.loom()', () => {
    it('should return a CUT decision', () => {
      assert.equal(fates.loom(), CUT)
    })
  })
  describe('when the loom method is envoked with ALIVE arguments, e.g. Fates.loom(ALIVE, 0..8)', () => {
    it('should return a CUT decision for 0 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 0), CUT)
    })
    it('should return a CUT decision for 1 neighbor', () => {
      assert.equal(fates.loom(ALIVE, 1), CUT)
    })
    it('should return a DRAWOUT decision for 2 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 2), DRAWOUT)
    })
    it('should return a DRAWOUT decision for 3 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 3), DRAWOUT)
    })
    it('should return a CUT decision for 4 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 4), CUT)
    })
    it('should return a CUT decision for 5 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 5), CUT)
    })
    it('should return a CUT decision for 6 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 6), CUT)
    })
    it('should return a CUT decision for 7 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 7), CUT)
    })
    it('should return a CUT decision for 8 neighbors', () => {
      assert.equal(fates.loom(ALIVE, 8), CUT)
    })
  })
  describe('when the loom method is envoked with DEAD arguments, e.g. Fates.loom(DEAD, 0..8)', () => {
    it('should return a CUT decision for 0 neighbors', () => {
      assert.equal(fates.loom(DEAD, 0), CUT)
    })
    it('should return a CUT decision for 1 neighbor', () => {
      assert.equal(fates.loom(DEAD, 1), CUT)
    })
    it('should return a CUT decision for 2 neighbors', () => {
      assert.equal(fates.loom(DEAD, 2), CUT)
    })
    it('should return a DRAWOUT decision for 3 neighbors', () => {
      assert.equal(fates.loom(DEAD, 3), DRAWOUT)
    })
    it('should return a CUT decision for 4 neighbors', () => {
      assert.equal(fates.loom(DEAD, 4), CUT)
    })
    it('should return a CUT decision for 5 neighbors', () => {
      assert.equal(fates.loom(DEAD, 5), CUT)
    })
    it('should return a CUT decision for 6 neighbors', () => {
      assert.equal(fates.loom(DEAD, 6), CUT)
    })
    it('should return a CUT decision for 7 neighbors', () => {
      assert.equal(fates.loom(DEAD, 7), CUT)
    })
    it('should return a CUT decision for 8 neighbors', () => {
      assert.equal(fates.loom(DEAD, 8), CUT)
    })
  })
})
