import { EmptyMock, Limits } from '../test/data/MooreNeighborhoodMock.mjs'
import Origin from './Origin.mjs'
import World from './World.mjs'
import { assert } from 'chai'
import mocha from 'mocha'

const { beforeEach, describe, it } = mocha

describe('processor/Neighbor/Origin', () => {
  const {
    top: {
      left: { coordinates }
    }
  } = EmptyMock
  beforeEach(() => {
    World.boundaries(Limits)
    Origin.set(coordinates)
  })

  describe('static origin coordinates', () => {
    it(`should return set coordinates e.g. ${JSON.stringify(
      coordinates
    )}`, () => {
      assert.equal(Origin.y, 0, 'y of i')
      assert.equal(Origin.x, 0, 'x of j')
    })
  })
})
