import East from './Neighbor/East.mjs'
import North from './Neighbor/North.mjs'
import NorthEast from './Neighbor/NorthEast.mjs'
import NorthWest from './Neighbor/NorthWest.mjs'
import Origin from './Origin.mjs'
import South from './Neighbor/South.mjs'
import SouthEast from './Neighbor/SouthEast.mjs'
import SouthWest from './Neighbor/SouthWest.mjs'
import West from './Neighbor/West.mjs'
import World from './World.mjs'

/**
 * Community: facade the logic for obtaining the Moore Neighborhood of Cell(ij)
 * +----+----+----+
 * | nw | n  | ne |
 * +----+----+----+
 * | w  | ij | e  |
 * +----+----+----+
 * | sw | s  | se |
 * +----+----+----+
 * see: https://en.wikipedia.org/wiki/Moore_neighborhood
 */
export default class Neighborhood {
  #members = []
  constructor(origin = { y: -1, x: -1 }, limits = { m: -1, n: -1 }) {
    World.boundaries(limits)
    Origin.set(origin)
    this.#members = [
      new East(),
      new North(),
      new NorthEast(),
      new NorthWest(),
      new South(),
      new SouthEast(),
      new SouthWest(),
      new West()
    ]
  }

  /**
   * get #members value
   * @return {array} - e.g. Community.members
   */
  get members() {
    return this.#members
  }
}
