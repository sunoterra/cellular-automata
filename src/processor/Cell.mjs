/**
 * Cell: individual within the simulation's 2D boundaries
 *       /                 \
 *       |     m       n   |
 * Aij = | (Ai)    (Aj)    |
 *       |     i=0     j=0 |
 *       \                 /
 */
export default class Cell {
  #age = 0
  #alive = false
  #coordinates = [-2, -2]
  /**
   * new Cell
   * @param {object} args - arguments wrapper
   * @param {number} args.y - Ai value
   * @param {number} args.x - Aj value
   */
  constructor([y, x] = []) {
    this.#coordinates = [y >= 0 ? y : -1, x >= 0 ? x : -1]
  }

  /**
   * get #age value
   * @return {number} - e.g. Cell.age
   */
  get age() {
    return this.#age
  }

  /**
   * get #alive value
   * @return {boolean} - e.g. Cell.alive
   */
  get alive() {
    return this.#alive
  }

  /**
   * get #coordinates value
   * @return {object} { y: (Ai..Am), x: (Aj..An) } - e.g. Cell.coordinates
   */
  get coordinates() {
    return this.#coordinates
  }

  /**
   * set #alive value; additionally
   * - if #alive === condition then increment #age
   * - if #alive does not strict-equal condition then set #age = 0
   * @param {boolean} condition - e.g. Cell.alive = Boolean; toggle alive state and determine #age value
   * @return {undefined}
   */
  set alive(condition) {
    this.#alive === condition ? this.#age++ : (this.#age = 0)
    this.#alive = condition
  }
}

// vim:foldlevel=2
