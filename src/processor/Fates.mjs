/**
 * Fates: impersonating those three Goddesses that put the "fear of God",
 * in God ...
 */
export default class Fates {
  #cut = false
  #drawout = true
  /**
   * loom: for spinning, drawing-out and cutting Life-threads
   * @param {boolean} thread - ALIVE = true, DEAD = false
   * @param {number} neighbors - neighbor "byte instruction"
   * @return {boolean} - CUT = false, DRAWOUT = true
   */
  loom(thread = false, neighbors = 0) {
    // see: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules
    // S23H4
    // live, 2 or 3 live neighbors; will live on
    // dead, 3 live neighbors; becomes alive by reproduction
    if ((thread === this.#drawout && neighbors === 2) || neighbors === 3) {
      return this.#drawout
    }
    // less then 2 neighbors; die by underpopulation
    // more than 3 neighbors; die by overpopulation
    return this.#cut
  }
}
