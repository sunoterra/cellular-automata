export default class Neighborhood {
  #members
  constructor(members = []) {
    this.update(members)
  }
  hail() {
    const fates = this.#members.map((member) => (member.fate ? 1 : 0))
    return fates.reduce((previous, current) => previous + current, 0)
  }
  update(members) {
    this.#members = members
  }
}
