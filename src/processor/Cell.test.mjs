import Cell from './Cell.mjs'
import { assert } from 'chai'
import mocha from 'mocha'

const { describe, it } = mocha

describe('processor/Cell', () => {
  describe('when constructor has no argument, e.g. `new Cell()`', () => {
    const cell = new Cell()
    it('should have prop age get return of 0', () => {
      assert.equal(cell.age, 0)
    })
    it('should have prop alive get return of false', () => {
      assert.equal(cell.alive, false)
    })
    it('should have prop coordinates get return of `[-1, -1]`', () => {
      assert.deepEqual(cell.coordinates, [-1, -1])
    })
  })
  describe('when constructor has [y, x] coordinates argument, e.g. `new Cell([0, 0])`', () => {
    const cell = new Cell([0, 0])
    it('should have prop age get return of 0', () => {
      assert.equal(cell.age, 0)
    })
    it('should have prop alive get return of false', () => {
      assert.equal(cell.alive, false)
    })
    it('should prop coordinates get return of `[0, 0]`', () => {
      assert.deepEqual(cell.coordinates, [0, 0])
    })
  })
  describe('when prop alive set is called with boolean', () => {
    const cell = new Cell([9, 16])
    it('should increment age prop value when boolean argument is strict-equal to current alive prop value', () => {
      cell.alive = false
      assert.equal(cell.age, 1)
    })
    it('should reset age prop value to 0 when boolean argument is opposite of current alive prop value', () => {
      cell.alive = true
      assert.equal(cell.age, 0)
    })
  })
})
