import { EmptyMock, Limits } from '../test/data/MooreNeighborhoodMock.mjs'
import Neighborhood from './Neighborhood.mjs'
import { assert } from 'chai'
import mocha from 'mocha'

const { beforeEach, describe, it } = mocha

const getMockNeighborsFor = (cell) =>
  Object.entries(cell)
    .map(([key, value]) => key !== 'coordinates' && value)
    .filter((value) => value !== false)

describe('processor/Neighborhood', () => {
  const { bottom, center, top } = EmptyMock
  let community
  let leftNeighbors
  let middleNeighbors
  let rightNeighbors

  describe('row y = 0', () => {
    const { left, middle, right } = top
    beforeEach(() => {
      leftNeighbors = getMockNeighborsFor(left)
      middleNeighbors = getMockNeighborsFor(middle)
      rightNeighbors = getMockNeighborsFor(right)
    })

    it(`should have members:\n${JSON.stringify(
      getMockNeighborsFor(left)
    )}\nfor column x = 0`, () => {
      community = new Neighborhood(left.coordinates, Limits)
      assert.deepEqual(community.members, leftNeighbors)
    })
    it(`should have members:\n${JSON.stringify(
      getMockNeighborsFor(middle)
    )}\nfor column x = 1`, () => {
      community = new Neighborhood(middle.coordinates, Limits)
      assert.deepEqual(community.members, middleNeighbors)
    })
    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(right)
    )}\nfor column x = 2`, () => {
      community = new Neighborhood(right.coordinates, Limits)
      assert.deepEqual(community.members, rightNeighbors)
    })
  })

  describe('row y = 1', () => {
    const { left, middle, right } = center
    beforeEach(() => {
      leftNeighbors = getMockNeighborsFor(left)
      middleNeighbors = getMockNeighborsFor(middle)
      rightNeighbors = getMockNeighborsFor(right)
    })

    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(left)
    )}\nfor column x = 0`, () => {
      community = new Neighborhood(left.coordinates, Limits)
      assert.deepEqual(community.members, leftNeighbors)
    })
    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(middle)
    )}\nfor column x = 1`, () => {
      community = new Neighborhood(middle.coordinates, Limits)
      assert.deepEqual(community.members, middleNeighbors)
    })
    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(right)
    )}\nfor column x = 2`, () => {
      community = new Neighborhood(right.coordinates, Limits)
      assert.deepEqual(community.members, rightNeighbors)
    })
  })

  describe('row y = 2', () => {
    const { left, middle, right } = bottom
    beforeEach(() => {
      leftNeighbors = getMockNeighborsFor(left)
      middleNeighbors = getMockNeighborsFor(middle)
      rightNeighbors = getMockNeighborsFor(right)
    })

    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(left)
    )}\nfor column x = 0`, () => {
      community = new Neighborhood(left.coordinates, Limits)
      assert.deepEqual(community.members, leftNeighbors)
    })
    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(middle)
    )}\nfor column x = 1`, () => {
      community = new Neighborhood(middle.coordinates, Limits)
      assert.deepEqual(community.members, middleNeighbors)
    })
    it(`should have expected members:\n${JSON.stringify(
      getMockNeighborsFor(right)
    )}`, () => {
      community = new Neighborhood(right.coordinates, Limits)
      assert.deepEqual(community.members, rightNeighbors)
    })
  })
})
