import Cell from 'Cell'
import Fates from 'Fates'
import Neighborhood from 'Neighborhood'

export default class ProcessorCell extends Cell {
  #fates = new Fates()
  #neighbors = new Neighborhood()

  constructor(Ai, Aj, members = []) {
    super(Ai, Aj)
    this.#neighbors.update(members)
  }

  #learnFate() {
    return this.#fates.loom(super.alive, this.#neighbors.hail())
  }
}
