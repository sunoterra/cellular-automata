// esm default
module.exports = {
  env: {
    browser: true,
    es6: true,
    node: true
  },
  extends: [
    'recommended/esnext',
    'recommended/esnext/style-guide',
    'recommended/node',
    'recommended/node/style-guide',
    'plugin:compat/recommended',
    'plugin:import/recommended',
    'plugin:node/recommended',
    'plugin:prettier/recommended',
    'plugin:promise/recommended'
  ],
  overrides: [
    // commonjs files
    {
      files: ['./.*rc.cjs', './src/test/**/*.cjs'],
      rules: {
        'import/no-commonjs': 'off',
        'node/no-unpublished-require': [
          'error',
          {
            allowModules: ['chai', 'mocha', 'sinon']
          }
        ]
      }
    }
  ],
  parser: 'espree',
  parserOptions: {
    ecmaVersion: 'latest',
    sourceType: 'module'
  },
  plugins: ['compat', 'import', 'node', 'prettier', 'promise'],
  root: true,
  rules: {
    'import/extensions': 'off',
    'node/no-missing-import': [
      'error',
      {
        allowModules: ['chai', 'mocha', 'serve-handler', 'sinon']
      }
    ],
    'node/no-unpublished-import': [
      'error',
      {
        allowModules: ['chai', 'mocha', 'serve-handler', 'sinon']
      }
    ]
  }
}
