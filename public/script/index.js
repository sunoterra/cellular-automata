;(() => {
  const { CellularAutomaton } = self
  // loaded; initialize
  document.addEventListener(`DOMContentLoaded`, ({ target }) => {
    CellularAutomaton.init(target)
  })
  // if interval is still running on page unload; kill it
  document.addEventListener(`onbeforeunload`, () => {
    const {
      clock: { id: clockId }
    } = CellularAutomaton
    if (clockId) {
      clearInterval(clockId)
    }
  })
})()
