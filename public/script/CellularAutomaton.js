// ..
;((factory) => {
  self.CellularAutomaton = factory()
})(() => {
  // const alive = []
  const cells = {
    alive: 0,
    dead: 0,
    randomAliveLimit: 0,
    randomAlivePercentage: 0.07,
    height: 8,
    total: 0,
    width: 8
  }
  const clock = {
    id: 0,
    interval: 125
  }
  const colors = {
    // alive: `#268bd2`, // blue
    // alive: `#2aa198`, // cyan
    // alive: `#859900`, // green
    alive: `#b58900`, // yellow
    // alive: `#cb4b16`, // orange
    // alive: `#dc322f`, // red
    // alive: `#d33682`, // magenta
    // alive: `#6c71c4`, // violet
    dead: `#fdf6e3` // base3 (light)
    // dead: `#002b36` // base03 (dark)
  }
  const generations = {
    count: -1,
    current: null,
    storage: null
  }
  const medium = {
    canvas: null,
    context: null,
    matrix: { i: 80, j: 120 }
  }
  const ui = {
    cells: null,
    dimensions: null,
    generations: null,
    buttons: {
      seed: null,
      play: null,
      next: null,
      pause: null
    }
  }
  let debug

  const debugDumpMatrix = (matrix) => {
    const {
      matrix: { j: An }
    } = medium
    let dump = '\n'
    for (const Ai of matrix.keys()) {
      for (const Aj of matrix[Ai].keys()) {
        dump += `${matrix[Ai][Aj]}`
        Aj === An - 1 ? (dump += `\n`) : (dump += `|`)
      }
    }
    return dump
  }

  const debugFunction = (title, matrix) => {
    console.group(title)
    console.info(`${debugDumpMatrix(matrix)}`)
    console.groupEnd()
  }

  const assembleMedium = (loadedDOM) => {
    medium.display = loadedDOM.getElementById(`display`)
    medium.canvas = loadedDOM.getElementById(`cellular-automata`)
    medium.context = medium.canvas.getContext(`2d`)
  }

  const updateUI = () => {
    ui.alive.innerHTML = `${cells.alive}`
    ui.dead.innerHTML = `${cells.dead}`
    ui.generations.innerHTML = `${generations.count}`
  }

  const cell = ({ Ai, Aj }) => {
    return generations.current[Ai][Aj]
  }

  const east = (Ai, Aj) => {
    const {
      matrix: { j: An }
    } = medium
    const j = 0
    const n = An - 1

    // [y, n]
    if (Aj === n) {
      // [y, j]
      return { Ai, Aj: j }
    }
    // otherwise [y, x + 1]
    return { Ai, Aj: Aj + 1 }
  }

  const north = (Ai, Aj) => {
    const {
      matrix: { i: Am }
    } = medium
    const i = 0
    const m = Am - 1

    // [i, x]
    if (Ai === i) {
      // [m, x]
      return { Ai: m, Aj }
    }
    // otherwise [y - 1, x]
    return { Ai: Ai - 1, Aj }
  }

  const northEast = (Ai, Aj) => {
    const {
      matrix: { i: Am, j: An }
    } = medium
    const i = 0
    const j = 0
    const m = Am - 1
    const n = An - 1

    if (Ai === i) {
      // [i, n]
      if (Aj === n) {
        // [m, j]
        return { Ai: m, Aj: j }
      }
      // [i, x != n]
      if (Aj !== n) {
        // [m, x + 1]
        return { Ai: m, Aj: Aj + 1 }
      }
    }
    // [y != i, n]
    if (Ai !== i && Aj === n) {
      // [y - 1, j]
      return { Ai: Ai - 1, Aj: j }
    }
    // otherwise [y - 1, x + 1]
    return { Ai: Ai - 1, Aj: Aj + 1 }
  }

  const northWest = (Ai, Aj) => {
    const {
      matrix: { i: Am, j: An }
    } = medium
    const i = 0
    const m = Am - 1
    const j = 0
    const n = An - 1

    if (Ai === i) {
      // [i, j]
      if (Aj === j) {
        // [m, n]
        return { Ai: m, Aj: n }
      }
      // [i, x != j]
      if (Aj !== j) {
        // [m, x - 1]
        return { Ai: m, Aj: Aj - 1 }
      }
    }
    // [y != i, j]
    if (Ai !== i && Aj === j) {
      // [y - 1, n]
      return { Ai: Ai - 1, Aj: n }
    }
    // otherwise [y -1, x - 1]
    return { Ai: Ai - 1, Aj: Aj - 1 }
  }

  const south = (Ai, Aj) => {
    const {
      matrix: { i: Am }
    } = medium
    const i = 0
    const m = Am - 1

    // [m, x]
    if (Ai === m) {
      // [i, x]
      return { Ai: i, Aj }
    }
    // otherwise [y + 1, x]
    return { Ai: Ai + 1, Aj }
  }

  const southEast = (Ai, Aj) => {
    const {
      matrix: { i: Am, j: An }
    } = medium
    const i = 0
    const m = Am - 1
    const j = 0
    const n = An - 1

    if (Ai === m) {
      // [m, n]
      if (Aj === n) {
        // [i, j]
        return { Ai: i, Aj: j }
      }
      // [m, x != n]
      if (Aj !== n) {
        // [i, x + 1]
        return { Ai: i, Aj: Aj + 1 }
      }
    }
    // [y != m, n]
    if (Ai !== m && Aj === n) {
      // [y + 1, j]
      return { Ai: Ai + 1, Aj: j }
    }
    // otherwise [y + 1, x + 1]
    return { Ai: Ai + 1, Aj: Aj + 1 }
  }

  const southWest = (Ai, Aj) => {
    const {
      matrix: { i: Am, j: An }
    } = medium
    const i = 0
    const m = Am - 1
    const j = 0
    const n = An - 1

    if (Ai === m) {
      // [m, j]
      if (Aj === j) {
        // [i, n]
        return { Ai: i, Aj: n }
      }
      // [m, x != j]
      if (Aj !== j) {
        // [i, x - 1]
        return { Ai: i, Aj: Aj - 1 }
      }
    }
    // [y != m, j]
    if (Ai !== m && Aj === j) {
      // [y + 1, n]
      return { Ai: Ai + 1, Aj: n }
    }
    // otherwise [y + 1, x - 1]
    return { Ai: Ai + 1, Aj: Aj - 1 }
  }

  const west = (Ai, Aj) => {
    const {
      matrix: { j: An }
    } = medium
    const j = 0
    const n = An - 1

    // [y, j]
    if (Aj === j) {
      // [y, n]
      return { Ai, Aj: n }
    }
    // otherwise [y, x - 1]
    return { Ai, Aj: Aj - 1 }
  }

  const countNeighbors = (Ai, Aj) => {
    if (debug) {
      console.info(`-----`)
      console.info(
        `-${cell(northWest(Ai, Aj))}|${cell(north(Ai, Aj))}|${cell(
          northEast(Ai, Aj)
        )}-`
      )
      console.info(`-${cell(west(Ai, Aj))}|${Ai},${Aj}|${cell(east(Ai, Aj))}-`)
      console.info(
        `-${cell(southWest(Ai, Aj))}|${cell(south(Ai, Aj))}|${cell(
          southEast(Ai, Aj)
        )}-`
      )
    }
    return (
      cell(east(Ai, Aj)) +
      cell(north(Ai, Aj)) +
      cell(northEast(Ai, Aj)) +
      cell(northWest(Ai, Aj)) +
      cell(south(Ai, Aj)) +
      cell(southEast(Ai, Aj)) +
      cell(southWest(Ai, Aj)) +
      cell(west(Ai, Aj))
    )
  }

  // see: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules
  const cycleCell = (Ai, Aj) => {
    const { current } = generations
    const aliveNeighbors = countNeighbors(Ai, Aj)
    // alive cells with 2 or 3 live neighbors will live on
    // dead cells with 3 live neighbors becomes alive by reproduction
    if (
      (current[Ai][Aj] === 1 && aliveNeighbors === 2) ||
      aliveNeighbors === 3
    ) {
      return 1
    }
    // alive cells with less then 2 neighbors die by underpopulation
    // alive cells with more than 3 neighbors die by overpopulation
    return 0
  }

  const draw = () => {
    const { height, width } = cells
    const { alive, dead } = colors
    const { current } = generations
    const {
      matrix: { i: Am, j: An }
    } = medium

    for (const Ai of current.keys()) {
      for (const Aj of current[Ai].keys()) {
        if (current[Ai][Aj] === 1) {
          medium.context.fillStyle = alive
        } else {
          medium.context.fillStyle = dead
        }
        const h = Ai === Am - 1 ? height : height - 1
        const w = Aj === An - 1 ? width : width - 1
        const x = Aj * width
        const y = Ai * height
        medium.context.fillRect(x, y, w, h)
      }
    }
    updateUI()
  }

  const lifecycle = () => {
    const { current } = generations
    const next = []

    cells.alive = 0
    cells.dead = 0
    for (const Ai of current.keys()) {
      next[Ai] = []
      for (const Aj of current[Ai].keys()) {
        const lifeSign = cycleCell(Ai, Aj)
        if (lifeSign) cells.alive++
        next[Ai][Aj] = lifeSign
      }
    }
    cells.dead = cells.total - cells.alive
    generations.count++
    generations.current = next
    generations.storage.push(next)

    if (debug) debugFunction(`lifecycle`, next)
  }

  const next = () => {
    Promise.all([lifecycle()])
      .then(() => {
        draw()
        return
      })
      .catch((thrown) => console.error(thrown))
  }

  const play = () => {
    const { interval } = clock
    if (!clock.id) {
      clock.id = setInterval(() => next(), interval)
    }
  }

  const pause = () => {
    if (clock.id) {
      clearInterval(clock.id)
      clock.id = 0
    }
  }

  const coordinates = {
    random: (min, max) =>
      min <= max ? min + Math.round(Math.random() * (max - min)) : 0
  }

  const loadConstruct = () => {
    const {
      matrix: { i: Am, j: An }
    } = medium
    const next = []

    cells.dead = Am * An
    cells.total = cells.dead
    for (let i = 0; i < Am; i++) {
      next[i] = []
      for (let j = 0; j < An; j++) {
        next[i][j] = 0
      }
    }
    generations.count = 0
    generations.current = next
    generations.storage = []
    ui.alive.innerHTML = `0`
    ui.dead.innerHTML = `${cells.total}`
    ui.total.innerHTML = `${cells.total}`
    ui.dimensions.innerHTML = `(m = ${Am}, n = ${An})`
    ui.generations.innerHTML = `0`

    // debugFunction(`loadConstruct: ${Am}h x ${An}w`, generations.next)
  }

  const randomSeed = () => {
    const { randomAlivePercentage } = cells
    const {
      matrix: { i: Am, j: An }
    } = medium
    const next = generations.current

    const aliveLimit = Math.floor(An * Am * randomAlivePercentage)
    for (let k = 0; k < aliveLimit; k++) {
      const i = coordinates.random(0, Am - 1)
      const j = coordinates.random(0, An - 1)
      if (next[i][j] === 0) {
        next[i][j] = 1
        cells.alive++
      }
    }

    generations.current = next
    generations.storage.push(next)
    const percent = Math.round(randomAlivePercentage * 100)
    debugFunction(`randomSeed: limit alive to ~${percent}%`, next)
  }

  const seed = () => {
    loadConstruct()
    randomSeed()
    draw()
  }

  const configureUI = (loadedDOM) => {
    ui.buttons.next = loadedDOM.getElementById('next')
    ui.buttons.pause = loadedDOM.getElementById('pause')
    ui.buttons.play = loadedDOM.getElementById('play')
    ui.buttons.seed = loadedDOM.getElementById('seed')
    ui.alive = loadedDOM.getElementById('alive')
    ui.dead = loadedDOM.getElementById('dead')
    ui.total = loadedDOM.getElementById('total')
    ui.dimensions = loadedDOM.getElementById('dimensions')
    ui.generations = loadedDOM.getElementById('generations')
    // events
    ui.buttons.next.addEventListener('click', (e) => {
      e.preventDefault()
      next()
    })
    ui.buttons.pause.addEventListener('click', (e) => {
      e.preventDefault()
      pause()
    })
    ui.buttons.play.addEventListener('click', (e) => {
      e.preventDefault()
      play()
    })
    ui.buttons.seed.addEventListener('click', (e) => {
      e.preventDefault()
      seed()
    })
  }

  return {
    clock,
    init: (loadedDOM, isDebug = false) => {
      debug = isDebug
      assembleMedium(loadedDOM)
      configureUI(loadedDOM)
      loadConstruct()
      randomSeed()
      draw()
    },
    seed,
    play,
    next,
    pause
  }
})
