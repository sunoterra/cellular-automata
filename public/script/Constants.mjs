const Cells = {
  height: 8,
  seed: 0.11,
  total: 9600,
  width: 8
}

const Colors = {
  hex: {
    blue: `#268bd2`,
    cyan: `#2aa198`,
    green: `#859900`,
    magenta: `#d33682`,
    orange: `#cb4b16`,
    red: `#dc322f`,
    violet: `#6c71c4`,
    yellow: `#b58900`,
    baseOne: `#93a1a1`,
    baseThree: `#fdf6e3`,
    baseTwo: `#eee8d5`,
    baseZeroThree: `#002b36`,
    baseZeroTwo: `#073642`,
    white: `#fff`
  },
  rgb: {
    baseThree: { r: 253, g: 246, b: 227 },
    baseTwo: { r: 238, g: 232, b: 213 },
    white: { r: 255, g: 255, b: 255 }
  }
}

const Default = {
  context: `2d`,
  countNumber: -1,
  countString: `0`,
  elements: {
    canvas: `canvas`
  }
}

const Id = {
  alive: `alive`,
  canvas: `cellular-automata`,
  dead: `dead`,
  dimensions: `dimensions`,
  generations: `generations`,
  next: `next`,
  pause: `pause`,
  play: `play`,
  seed: `seed`,
  total: `total`
}

const Event = {
  Click: `click`
}

const World = {
  i: 0,
  j: 0,
  m: 79,
  n: 119
}

export { Cells, Colors, Default, Event, Id, World }
