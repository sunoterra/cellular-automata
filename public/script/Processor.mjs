import { Cells, World } from './Constants.mjs'
import {
  east,
  north,
  northEast,
  northWest,
  south,
  southEast,
  southWest,
  west
} from './Neighbors.mjs'

const debugDumpMatrix = (matrix) => {
  const { n } = World
  let dump = ''
  for (const y of matrix.keys()) {
    for (const x of matrix[y].keys()) {
      dump += `${matrix[y][x][0]}`
      if (x === n) {
        dump += `\n`
      }
    }
  }
  return dump
}

const debugFunction = (title, matrix) => {
  console.group(title)
  console.info(`${debugDumpMatrix(matrix)}`)
  console.groupEnd()
}

const cell = (matrix, [y, x]) => matrix[y][x][0]

const coordinate = {
  random: (min, max) =>
    min <= max ? min + Math.round(Math.random() * (max - min)) : 0
}

const countNeighbors = (matrix, coordinates) => {
  return (
    cell(matrix, east(coordinates)) +
    cell(matrix, north(coordinates)) +
    cell(matrix, northEast(coordinates)) +
    cell(matrix, northWest(coordinates)) +
    cell(matrix, south(coordinates)) +
    cell(matrix, southEast(coordinates)) +
    cell(matrix, southWest(coordinates)) +
    cell(matrix, west(coordinates))
  )
}

// see: https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life#Rules
const cycleCell = (matrix, coordinates) => {
  const aliveNeighbors = countNeighbors(matrix, coordinates)
  const [y, x] = coordinates
  const [vital, age] = matrix[y][x]
  // alive cells with 2 or 3 live neighbors will live on
  // dead cells with 3 live neighbors becomes alive by reproduction
  if ((vital === 1 && aliveNeighbors === 2) || aliveNeighbors === 3) {
    return [1, vital === 0 ? 0 : age < 24 ? age + 1 : age]
  }
  // alive cells with less then 2 neighbors die by underpopulation
  // alive cells with more than 3 neighbors die by overpopulation
  return [0, vital === 1 ? 0 : age < 24 ? age + 1 : age]
}

const Lifecycle = (generation, debug) => {
  const next = []
  let alive = 0

  for (const y of generation.keys()) {
    next[y] = []
    for (const x of generation[y].keys()) {
      const [vital, age] = cycleCell(generation, [y, x])
      if (vital) alive++
      next[y][x] = [vital, age]
    }
  }

  if (debug) debugFunction(`Lifecycle`, next)
  return {
    alive,
    generation: next
  }
}

const Construct = () => {
  const { m, n } = World
  const matrix = []

  for (let i = 0; i <= m; i++) {
    matrix[i] = []
    for (let j = 0; j <= n; j++) {
      matrix[i][j] = [0, 23]
    }
  }
  return matrix
}

const Seed = (matrix) => {
  const { seed, total } = Cells
  const { m, n } = World
  let alive = 0

  const aliveLimit = Math.floor(m * n * seed)
  for (let k = 0; k < aliveLimit; k++) {
    const i = coordinate.random(0, m)
    const j = coordinate.random(0, n)
    if (matrix[i][j][0] === 0) {
      matrix[i][j] = [1, 0]
      alive++
    }
  }

  const percent = Math.round(seed * 100)
  debugFunction(`Seed: ~${percent}% - ${alive}/${total}`, matrix)
  return {
    alive,
    generation: matrix
  }
}

export { Construct, Lifecycle, Seed }
