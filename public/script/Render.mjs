import { Cells, Colors, Default, Id, World } from './Constants.mjs'

const onClickHandlerFactory = (action) => {
  return (e) => {
    e.preventDefault()
    action()
  }
}

class Buttons {
  next
  pause
  play
  seed
  constructor(doc) {
    this.next = doc.getElementById(Id.next)
    this.pause = doc.getElementById(Id.pause)
    this.play = doc.getElementById(Id.play)
    this.seed = doc.getElementById(Id.seed)
  }
}

class Medium {
  #canvas
  #context
  #matrix
  #buildCache(doc) {
    const {
      hex: { baseThree, blue, cyan, green, violet, yellow },
      rgb: { white }
    } = Colors
    // alive
    this.#cacheEntry(doc, `adult`, green)
    this.#cacheEntry(doc, `child`, blue)
    this.#cacheEntry(doc, `elder`, yellow)
    this.#cacheEntry(doc, `infant`, violet)
    this.#cacheEntry(doc, `teen`, cyan)
    // dead
    this.#cacheEntry(doc, `ancient`, baseThree)
    this.#cacheEntry(
      doc,
      `ancestor0`,
      `rgba(${white.r}, ${white.g}, ${white.b}, 1)`,
      this.#canvas.ancient
    )
    this.#cacheEntry(
      doc,
      `ancestor1`,
      `rgba(${white.r}, ${white.g}, ${white.b}, 0.8)`,
      this.#canvas.ancient
    )
    this.#cacheEntry(
      doc,
      `ancestor2`,
      `rgba(${white.r}, ${white.g}, ${white.b}, 0.5)`,
      this.#canvas.ancient
    )
    this.#cacheEntry(
      doc,
      `ancestor3`,
      `rgba(${white.r}, ${white.g}, ${white.b}, 0.2)`,
      this.#canvas.ancient
    )
  }
  #cacheEntry(doc, name, fillStyle, bg) {
    const { height, width } = Cells
    const {
      context,
      elements: { canvas }
    } = Default
    const h = height - 1
    const w = width - 1
    this.#canvas[name] = doc.createElement(canvas)
    const cell = this.#canvas[name].getContext(context)
    if (bg) {
      cell.drawImage(bg, 0, 0)
    }
    cell.fillStyle = fillStyle
    cell.fillRect(0, 0, w, h)
  }
  constructor(doc) {
    this.#canvas = doc.getElementById(Id.canvas)
    // TODO: implement viewport orientation detection and then ...
    // see:
    // https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Optimizing_canvas#scaling_for_high_resolution_displays
    // const dpr = window.devicePixelRatio
    // const surface = this.#canvas.getBoundingClientRect()
    // this.#canvas.height = surface.height * dpr
    // this.#canvas.width = surface.width * dpr
    this.#context = this.#canvas.getContext(Default.context)
    // this.#context.scale(dpr, dpr)
    // this.#canvas.style.width = `${surface.width}px`
    // this.#canvas.style.height = `${surface.height}px`
    this.#buildCache(doc)
    this.#matrix = null
  }
  draw() {
    const { height, width } = Cells
    const {
      adult,
      ancient,
      ancestor0,
      ancestor1,
      ancestor2,
      ancestor3,
      child,
      elder,
      height: canvasHeight,
      infant,
      teen,
      width: canvasWidth
    } = this.#canvas

    this.#context.clearRect(0, 0, canvasWidth, canvasHeight)
    for (const i of this.#matrix.keys()) {
      for (const j of this.#matrix[i].keys()) {
        const [vital, age] = this.#matrix[i][j]
        const x = j * width
        const y = i * height

        if (vital === 1) {
          if (age >= 0 && age < 3) {
            this.#context.drawImage(infant, x, y)
          }
          if (age >= 3 && age < 7) {
            this.#context.drawImage(child, x, y)
          }
          if (age >= 7 && age < 13) {
            this.#context.drawImage(teen, x, y)
          }
          if (age >= 13 && age < 23) {
            this.#context.drawImage(adult, x, y)
          }
          if (age >= 23) {
            this.#context.drawImage(elder, x, y)
          }
        } else {
          if (age >= 0 && age < 3) {
            this.#context.drawImage(ancestor0, x, y)
          }
          if (age >= 3 && age < 7) {
            this.#context.drawImage(ancestor1, x, y)
          }
          if (age >= 7 && age < 13) {
            this.#context.drawImage(ancestor2, x, y)
          }
          if (age >= 13 && age < 23) {
            this.#context.drawImage(ancestor3, x, y)
          }
          if (age >= 23) {
            this.#context.drawImage(ancient, x, y)
          }
        }
      }
    }
  }
  update({ current }) {
    this.#matrix = current
  }
}

class Menu {
  #alive
  #dead
  #dimensions
  #generations
  #total
  constructor(doc) {
    this.#alive = doc.getElementById(Id.alive)
    this.#alive.innerHTML = Default.countString
    this.#dead = doc.getElementById(Id.dead)
    this.#dead.innerHTML = Cells.total
    this.#dimensions = doc.getElementById(Id.dimensions)
    this.#dimensions.innerHTML = `(m = ${World.m + 1}, n = ${World.n + 1})`
    this.#generations = doc.getElementById(Id.generations)
    this.#generations.innerHTML = Default.countString
    this.#total = doc.getElementById(Id.total)
    this.#total.innerHTML = Cells.total
  }
  set alive(count = Default.countString) {
    this.#alive.innerHTML = count
  }
  set dead(count = Default.countString) {
    this.#dead.innerHTML = count
  }
  set dimensions(spec = '[m, n]') {
    this.#dimensions.innerHTML = spec
  }
  set generations(count = Default.countString) {
    this.#generations.innerHTML = count
  }
  set total(count = Default.countString) {
    this.#total.innerHTML = count
  }
  update({ alive, dead, count }) {
    this.alive = alive
    this.dead = dead
    this.generations = count
  }
}

export { Buttons, Medium, Menu, onClickHandlerFactory }
