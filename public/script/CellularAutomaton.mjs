import { Buttons, Medium, Menu, onClickHandlerFactory } from './Render.mjs'
import { Clock, Generations } from './State.mjs'
import { Construct, Lifecycle, Seed } from './Processor.mjs'
import { Event } from './Constants.mjs'

let buttons
let debug
let generations
let lastTimestamp
let medium
let menu

const next = () => {
  generations.update(Lifecycle(generations.current, debug))
  medium.update(generations)
  medium.draw()
  menu.update(generations)
}

const handleDrawFrame = (timestamp) => {
  const { interval } = Clock
  if (lastTimestamp === undefined) {
    lastTimestamp = timestamp
  }
  const delta = timestamp - lastTimestamp
  if (delta >= interval) {
    lastTimestamp = timestamp
    next()
  }
  Clock.id = window.requestAnimationFrame(handleDrawFrame)
}

const play = () => {
  next()
  Clock.id = window.requestAnimationFrame(handleDrawFrame)
}

const pause = () => {
  if (Clock.id !== 0) {
    window.cancelAnimationFrame(Clock.id)
    Clock.id = 0
    lastTimestamp = undefined
  }
}

const seed = () => {
  generations.reset()
  generations.load(Seed(Construct()))
  medium.update(generations)
  medium.draw()
  menu.update(generations)
}

const CellularAutomaton = {
  clock: Clock,
  init: (loadedDOM, isDebug = false) => {
    const { Click } = Event
    debug = isDebug

    buttons = new Buttons(loadedDOM)
    buttons.next.addEventListener(Click, onClickHandlerFactory(next))
    buttons.pause.addEventListener(Click, onClickHandlerFactory(pause))
    buttons.play.addEventListener(Click, onClickHandlerFactory(play))
    buttons.seed.addEventListener(Click, onClickHandlerFactory(seed))

    generations = new Generations()
    generations.load(Seed(Construct()))

    medium = new Medium(loadedDOM)
    medium.update(generations)
    medium.draw()

    menu = new Menu(loadedDOM)
    menu.update(generations)
  },
  next,
  play,
  pause,
  seed
}

export default CellularAutomaton
