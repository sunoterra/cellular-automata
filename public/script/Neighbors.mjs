import { World } from './Constants.mjs'

const east = ([y, x]) => {
  const { j, n } = World

  if (x === n) {
    return [y, j]
  }
  return [y, x + 1]
}

const north = ([y, x]) => {
  const { i, m } = World

  if (y === i) {
    return [m, x]
  }
  return [y - 1, x]
}

const northEast = ([y, x]) => {
  const { i, j, m, n } = World

  if (y === i) {
    if (x === n) {
      return [m, j]
    }
    if (x !== n) {
      return [m, x + 1]
    }
  }
  if (y !== i && x === n) {
    return [y - 1, j]
  }
  return [y - 1, x + 1]
}

const northWest = ([y, x]) => {
  const { i, j, m, n } = World

  if (y === i) {
    if (x === j) {
      return [m, n]
    }
    if (x !== j) {
      return [m, x - 1]
    }
  }
  if (y !== i && x === j) {
    return [y - 1, n]
  }
  return [y - 1, x - 1]
}

const south = ([y, x]) => {
  const { i, m } = World

  if (y === m) {
    return [i, x]
  }
  return [y + 1, x]
}

const southEast = ([y, x]) => {
  const { i, j, m, n } = World

  if (y === m) {
    if (x === n) {
      return [i, j]
    }
    if (x !== n) {
      return [i, x + 1]
    }
  }
  if (y !== m && x === n) {
    return [y + 1, j]
  }
  return [y + 1, x + 1]
}

const southWest = ([y, x]) => {
  const { i, j, m, n } = World

  if (y === m) {
    if (x === j) {
      return [i, n]
    }
    if (x !== j) {
      return [i, x - 1]
    }
  }
  if (y !== m && x === j) {
    return [y + 1, n]
  }
  return [y + 1, x - 1]
}

const west = ([y, x]) => {
  const { j, n } = World

  if (x === j) {
    return [y, n]
  }
  return [y, x - 1]
}

export { east, north, northEast, northWest, south, southEast, southWest, west }
