import { Cells, Default } from './Constants.mjs'

const Clock = {
  id: 0,
  // interval: 499
  // interval: 337
  // interval: 307
  // interval: 269
  // interval: 233
  // interval: 199
  // interval: 163
  interval: 127
  // interval: 97
  // interval: 67
  // interval: 37
}

class Generations {
  #alive
  #count
  #current
  #dead
  #storage
  constructor() {
    const { countNumber } = Default
    this.#alive = countNumber
    this.#count = countNumber
    this.#current = null
    this.#dead = countNumber
    this.#storage = []
  }
  get alive() {
    return this.#alive
  }
  get count() {
    return this.#count
  }
  get current() {
    return this.#current
  }
  get dead() {
    return this.#dead
  }
  set alive(count) {
    this.#alive = count
  }
  set count(count) {
    this.#count = count
  }
  set current(generation = []) {
    this.#current = generation
  }
  set dead(count) {
    this.#dead = count
  }
  load({ alive, generation }) {
    this.alive = alive
    this.count++
    this.current = generation
    this.dead = Cells.total - alive
    if (this.#storage.length > 1000) {
      this.#storage.pop()
    }
    this.#storage.push(generation)
  }
  reset() {
    const { countNumber } = Default
    this.alive = countNumber
    this.count = countNumber
    this.current = null
    this.dead = countNumber
    this.#storage = []
  }
  update(spec) {
    this.load(spec)
  }
}

export { Clock, Generations }
